# GitLab CI - Example

Projeto de exemplo utilizando [GitLab CI](https://docs.gitlab.com/ee/ci/)
como *continuous integration* e *continuous delivery*.

Jobs executados:

1. build: Cria o build do projeto.
2. lint: Faz uma análise estática do código.
3. test: Roda os testes e validações.
4. release: Da *push* da tag para o registry.
5. deploy: Envia para a produção.

# Release e Deploy

Para fazer a release e deploy para produção está sendo utilizado um git flow
com a geração de tags para o projeto, seguindo o padrão [semantic versioning](https://semver.org).

Ou seja, quando uma [tag](https://gitlab.com/rafaelbmateus/gitlab-ci-example/-/tags) for criada
o CI irá fazer o build da imagem docker, publicar para o
[registry](https://gitlab.com/rafaelbmateus/gitlab-ci-example/container_registry) com a versão da tag
criada e sobscrevendo a tag latest.
